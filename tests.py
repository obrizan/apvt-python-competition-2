from unittest import TestCase

from task1 import прямоугольный_ли_треугольник
from task2 import все_делители_числа
from task3 import сокращенная_дробь
from task4 import арифметическая_ли_прогрессия
from task5 import зашифровать


class Tests(TestCase):

    def test_task1(self):
        tests = [
            ((3, 4, 5), True),
            ((5, 4, 3), True),
            ((4, 3, 5), True),
            ((3, 3, 3), False)
        ]
        for test in tests:
            args = test[0]
            response = test[1]
            result = прямоугольный_ли_треугольник(*args)
            msg = "На аргументы {} ваш ответ {}, а правильный {}.".format(args, result, response)
            self.assertEqual(result, response, msg)

    def test_task2(self):
        tests = [
            (1, [1]),
            (2, [1, 2]),
            (3, [1, 3]),
            (4, [1, 2, 4]),
            (5, [1, 5]),
            (6, [1, 2, 3, 6]),
            (7, [1, 7]),
            (8, [1, 2, 4, 8]),
            (9, [1, 3, 9]),
            (10, [1, 2, 5, 10]),
            (11, [1, 11]),
            (12, [1, 2, 3, 4, 6, 12]),
            (20, [1, 2, 4, 5, 10, 20]),
            (35, [1, 5, 7, 35]),
            (100, [1, 2, 4, 5, 10, 20, 25, 50, 100])
        ]
        for test in tests:
            args = test[0]
            expected = test[1]
            actual = все_делители_числа(args)
            msg = "На аргументы {} ваш ответ {}, а правильный {}.".format(args, actual, expected)
            self.assertEqual(actual, expected, msg)

    def test_task3(self):
        tests = [
            ((1, 2), (1, 2)),
            ((1, 3), (1, 3)),
            ((2, 4), (1, 2)),
            ((4, 8), (1, 2)),
            ((8, 16), (1, 2)),
            ((3, 6), (1, 2)),
            ((6, 12), (1, 2)),
            ((12, 24), (1, 2)),
            ((48, 88), (6, 11)),
        ]
        for test in tests:
            args = test[0]
            expected = test[1]
            actual = сокращенная_дробь(*args)
            msg = "На аргументы {} ваш ответ {}, а правильный {}.".format(args, actual, expected)
            self.assertEqual(actual, expected, msg)

    def test_task4(self):
        tests = [
            ((1,), True),
            ((1, 1, 1), True),
            ((1, 2, 2), False),
            ((1, 1, 2), False),
            ((1, 2, 3), True),
            ((1, 3, 5, 7, 9), True),
            ((10, 9, 8, 7, 6), True),
            ((2, -2, -6, -10), True),
            ((1, 3, 5, 7, 9, 11, 12), False),
        ]
        for test in tests:
            args = test[0]
            expected = test[1]
            actual = арифметическая_ли_прогрессия(args)
            msg = "На аргументы {} ваш ответ {}, а правильный {}.".format(args, actual, expected)
            self.assertEqual(actual, expected, msg)

    def test_task5(self):
        tests = [
            ('зашифрованный текст.', 'засашисифросовасаннысый тесекст.'),
            ('привет.', 'присивесет.'),
            ('привет, дима!', 'присивесет, дисимаса!'),
            ('привет, сергей!', 'присивесет, сесергесей!'),
            ('как дела?', 'касак деселаса?'),
            ('хорошо.', 'хосоросошосо.'),
            ('а', 'аса'),
            ('тчк.', 'тчк.'),
            ('аеёиоуыэюя', 'асаесеёсёисиосоусуысыэсэюсюяся'),
            ('ооо', 'осоосоосо')
        ]
        for test in tests:
            args = test[0]
            expected = test[1]
            actual = зашифровать(args)
            msg = "На аргумент '{}' ваш ответ '{}', а правильный '{}'.".format(args, actual, expected)
            self.assertEqual(actual, expected, msg)
